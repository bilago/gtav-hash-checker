﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTAV_File_Checker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CollectionView view;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                if (!File.Exists("hashs.csv"))
                    updateHash();
                HashView.ItemsSource = fileList;
                //view = (CollectionView)CollectionViewSource.GetDefaultView(HashView.ItemsSource);
                //view.SortDescriptions.Add(new System.ComponentModel.SortDescription("Status", System.ComponentModel.ListSortDirection.Ascending));
                HashView.Items.SortDescriptions.Add(new SortDescription("Status", ListSortDirection.Ascending));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Crashed!");
            }
        }

        private void createDictionary()
        {
            GoodHashes.Clear();
            GoodFileSizes.Clear();
          string[] hashes = File.ReadAllLines("hashs.csv");
          foreach(string hash in hashes)
          {
              try
              {
                  string[] data = hash.Split(';');
                  if (data.Length != 3)
                      continue;

                  GoodHashes.Add(data[0], data[2]);
                  GoodFileSizes.Add(data[0], float.Parse(data[1]));
              }
              catch(Exception ex)
              {
                  //MessageBox.Show(ex.Message);
              }
              DoEvents();
          }

        }

        public static string InstallPath;
        public static Dictionary<string, string> GoodHashes = new Dictionary<string, string>();
        public static Dictionary<string, float> GoodFileSizes = new Dictionary<string, float>();
        public ObservableCollection<fileHashinfo> fileList { get; set; }

        public List<ThreadClass> threads = new List<ThreadClass>();

        public static byte[] getHash(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }

        
        private void CheckNow_Click(object sender, RoutedEventArgs e)
        {
            pleasewait.Visibility = System.Windows.Visibility.Visible;
            ButtonGrid.IsEnabled = false;

            BackgroundWorker b = new BackgroundWorker();
            b.DoWork += b_DoWork;
            b.RunWorkerCompleted += b_RunWorkerCompleted;
            b.RunWorkerAsync();

        }

        private void b_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                pleasewait.Visibility = System.Windows.Visibility.Hidden;
                ButtonGrid.IsEnabled = true;
            }));
        }

        private void b_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Dispatcher.BeginInvoke((Action)(() =>
                    {
                        if (fileList == null)
                            fileList = new ObservableCollection<fileHashinfo>();
                        else
                            fileList.Clear();

                        if (HashView.ItemsSource == null)
                            HashView.ItemsSource = fileList;
                    }));

                InstallPath = getInstallPath();
               
                //InstallPath = Environment.CurrentDirectory;
                createDictionary();

                if (string.IsNullOrWhiteSpace(InstallPath) || !Directory.Exists(InstallPath))
                    throw new Exception("Installation folder is not found. Please make sure you have read access to the registry or that GTAV is installed.");

                if (!File.Exists(System.IO.Path.Combine(InstallPath, "GTA5.exe")))
                    throw new Exception("Installation folder was found but GTA5.exe is missing. Please make sure your game is installed properly.");


                //foreach (string file in Directory.GetFiles(InstallPath, "*.*", SearchOption.AllDirectories))
                //{

                foreach (var item in GoodHashes)
                {
                    try
                    {
                        ThreadClass tclass = new ThreadClass();
                        tclass.t = new Thread(() =>
                        {
                            string file = item.Key.Replace("...", InstallPath);
                            tclass.Name = System.IO.Path.GetFileName(file);
                            Dispatcher.BeginInvoke((Action)(() => pleasewait.Text = string.Format("Checking file: {0}", tclass.Name)));

                            fileHashinfo fhash = new fileHashinfo();

                            fhash.FileName = System.IO.Path.GetFileName(item.Key);
                            fhash.FilePath = item.Key;
                            if (!File.Exists(file))
                            {
                                fhash.Status = "File Does not Exist!";
                                Dispatcher.BeginInvoke((Action)(() => fileList.Add(fhash)));
                                return;
                            }
                            string correctHash;
                            float correctSize = 0f;
                            fhash.FileSize = new FileInfo(file).Length;


                            GoodFileSizes.TryGetValue(item.Key, out correctSize);
                            if (correctSize == 0f)
                                fhash.Status = "File Not Found in Hash Database";
                            else if (correctSize != fhash.FileSize)
                                fhash.Status = "File size mismatch!";
                            if (string.IsNullOrWhiteSpace(fhash.Status))
                            {
                                try
                                {
                                    fhash.CurrentHash = BitConverter.ToString(getHash(file)).Replace("-", "");
                                    GoodHashes.TryGetValue(item.Key, out correctHash);
                                    if (string.IsNullOrWhiteSpace(correctHash))
                                        fhash.Status = "File Not Found in Hash Database";
                                    else if (correctHash == fhash.CurrentHash)
                                        fhash.Status = string.Format("Hash and file size are correct");
                                    else
                                        fhash.Status = string.Format("Hash is incorrect! ({0})", fhash.CurrentHash);
                                }
                                catch
                                {
                                    fhash.Status = "Unable to check the hash of this file";
                                }
                            }
                            lock (fileList)
                                Dispatcher.BeginInvoke((Action)(() =>
                                    {

                                        CollectionViewSource.GetDefaultView(HashView.ItemsSource).Refresh();
                                        ResetColumnWidths();
                                        fileList.Add(fhash);
                                    }));

                        });

                        tclass.t.IsBackground = true;
                        tclass.t.Start();
                        threads.Add(tclass);
                        Thread.Sleep(100);
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.ToString());
                    }

                }

                foreach (ThreadClass thread in threads)
                {
                    Dispatcher.BeginInvoke((Action)(() => pleasewait.Text = string.Format("Checking file: {0}", thread.Name)));
                    while (thread.t.IsAlive)
                    {
                        Dispatcher.BeginInvoke((Action)(() =>
                            {
                                //HashView.ItemsSource = fileList;
                                CollectionViewSource.GetDefaultView(HashView.ItemsSource).Refresh();
                            }));
                        Thread.Sleep(100);
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR");
            }
        }

        List<ColCheck> columnCheck = new List<ColCheck>();

        public class ColCheck
        {
            public GridViewColumn view { get; set; }
            public double width { get; set; }
        }
        public void ResetColumnWidths()
        {
            if (columnCheck.Count == 0)
            {
                columnCheck.Add(new ColCheck(){view = CurrentHashCol, width = 80.00});
                columnCheck.Add(new ColCheck() { view = FileSizeCol, width = 80.00 });
                columnCheck.Add(new ColCheck() { view = FilePathCol, width = 200.00 });
                columnCheck.Add(new ColCheck() { view = StatusCol, width = 80.00 });                
            }
            

            foreach (var col in columnCheck)
            {
                if (Double.IsNaN(col.view.Width))
                {
                    // force update will be triggered
                    col.view.Width = col.view.ActualWidth;
                }
                col.view.Width = Double.NaN;

                if (col.view.ActualWidth < col.width)
                {
                    col.view.Width = col.width;
                }
                else
                    col.width = col.view.ActualWidth;                    
            }
        }

        public static void DoEvents()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                                                  new Action(delegate { }));
        }
        public class ThreadClass
        {
            public Thread t { get; set; }
            public string Name { get; set; }
        }
        public class fileHashinfo
        {
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string CurrentHash { get; set; }
            public long FileSize { get; set; }
            public string Status { get; set; }

        }

        private string getInstallPath()
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rockstar games\Grand Theft Auto V"))
            {
                string path = string.Empty;
                if (key != null)
                    path = key.GetValue("InstallFolder", null).ToString();
                    return path;
            }
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VGGC35JQMJA88");
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Hyperlink_RequestNavigate_1(object sender, RequestNavigateEventArgs e)
        {
            Process.Start("http://www.reddit.com/r/GrandTheftAutoV_PC/comments/32yvn2/noob_friendly_gtav_hash_and_filesize_checker_by/");
        }

        private void UpdateHash_Click(object sender, RoutedEventArgs e)
        {
            updateHash();  
        }

        private void updateHash()
        {
            hashDownload.Visibility = System.Windows.Visibility.Visible;
            ButtonGrid.IsEnabled = false;
            using (WebClient Client = new WebClient())
            {
                Client.DownloadFileCompleted += Client_DownloadFileCompleted;
                File.Delete("hashs.csv");
                Client.DownloadFileAsync(new Uri("http://gtav-hashes.no-ip.net/GTAV/hashs.csv"), "hashs.csv");
            }
        }

        private void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            hashDownload.Visibility = System.Windows.Visibility.Hidden;
            ButtonGrid.IsEnabled = true;
        }

        private void HashView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;

        private void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                HashView.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            HashView.Items.SortDescriptions.Clear();
            HashView.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
            HashView.Items.Refresh();
          
        }

        

    }
    public class SortAdorner : Adorner
    {
        private static Geometry ascGeometry =
                Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        private static Geometry descGeometry =
                Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement element, ListSortDirection dir)
            : base(element)
        {
            this.Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            TranslateTransform transform = new TranslateTransform
                    (
                            AdornedElement.RenderSize.Width - 15,
                            (AdornedElement.RenderSize.Height - 5) / 2
                    );
            drawingContext.PushTransform(transform);

            Geometry geometry = ascGeometry;
            if (this.Direction == ListSortDirection.Descending)
                geometry = descGeometry;
            drawingContext.DrawGeometry(Brushes.Black, null, geometry);

            drawingContext.Pop();
        }
    }
}
