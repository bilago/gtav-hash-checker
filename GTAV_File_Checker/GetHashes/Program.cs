﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GetHashes
{
    class Program
    {
        static void Main(string[] args)
        {
            writeCSV();
        }

        static List<string> lines = new List<string>();
        static List<System.Threading.Thread> threads = new List<System.Threading.Thread>();
        private static void writeCSV()
        {

            foreach (string file in Directory.GetFiles(Environment.CurrentDirectory, "*.*", SearchOption.AllDirectories))
            {
                System.Threading.Thread thread = new System.Threading.Thread(() =>
                    {
                        if (file.Contains("hashs.csv") || file.Contains("Gethashes.exe"))
                            return;
                        Console.WriteLine("Getting Hash for file {0}", Path.GetFileName(file));
                        StringBuilder csvLine = new StringBuilder();

                        csvLine.Append(file.Replace(Environment.CurrentDirectory,"...")).Append(";").Append(new FileInfo(file).Length).Append(";").Append(BitConverter.ToString(getHash(file)).Replace("-", ""));
                        Console.WriteLine(csvLine);
                        lock (lines)
                            lines.Add(csvLine.ToString());
                    });

                thread.Start();
                threads.Add(thread);           

            }

            foreach (System.Threading.Thread thread in threads)
            {

                if (thread.IsAlive)
                    thread.Join();
            }

            using (StreamWriter write = new StreamWriter("hashs.csv", false))
            {
                foreach (string line in lines)
                {
                    write.WriteLine(line);
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

       

        public static byte[] getHash(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }
    }
}
